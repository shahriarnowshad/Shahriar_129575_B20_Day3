<?php
    echo "This is econventional tag";
    echo "<br/>";
    echo "we should use this";
    echo "<br/>";
    echo "for best practice";
    echo "<br/>";
    echo "<hr>";
?>

<?
    echo "This is short open tag";
    echo "<br/>";
    echo "It is off by default";
    echo "<br/>";
    echo "We edited php.ini file";
    echo "<br/>";
    echo "<hr>";
?>

<%
    echo "This is asp tag";
    echo "<br/>";
    echo "It is not used";
    echo "<br/>";
    echo "Edited php.ini file";
    echo "<br/>";
    echo "<hr>";

%>
<script language="php">
    echo "This is script tag";
    echo "<br/>";
    echo "This is less used";
     echo "<br/>";
     echo "It works though";
    echo "<br/>";
    echo "<hr>";
</script>

